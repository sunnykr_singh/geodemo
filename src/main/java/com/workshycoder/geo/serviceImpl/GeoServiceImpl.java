package com.workshycoder.geo.serviceImpl;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;

import org.springframework.stereotype.Service;

import com.maxmind.geoip2.DatabaseReader;
import com.maxmind.geoip2.exception.GeoIp2Exception;
import com.maxmind.geoip2.model.CityResponse;
import com.workshycoder.geo.exception.DATABASEEXCEPTION;
import com.workshycoder.geo.exception.IPNOTFOUNDEXCEPTION;
import com.workshycoder.geo.model.UserDetails;
import com.workshycoder.geo.service.GeoService;

@Service
public class GeoServiceImpl implements GeoService {

	private DatabaseReader dbReader;

	public GeoServiceImpl() throws DATABASEEXCEPTION {
		File database = new File(this.getClass().getResource("/GeoLite2-City.mmdb").getFile());
		try {
			dbReader = new DatabaseReader.Builder(database).build();
		} catch (IOException e) {
			throw new DATABASEEXCEPTION("Exception while building DataBaseReader");
		}
	}

	@Override
	public UserDetails getUsersAddress(String ip) throws IPNOTFOUNDEXCEPTION {
		UserDetails userDetails = new UserDetails();
		try {
			InetAddress ipAddress = InetAddress.getByName(ip);
			CityResponse response = dbReader.city(ipAddress);
			userDetails.setCity(response.getCity().getName())
					.setPostal(response.getPostal().getCode())
					.setLatitude(response.getLocation().getLatitude().toString())
					.setLongitude(response.getLocation().getLongitude().toString())
					.setIp(ip);
			return userDetails;
		} catch (GeoIp2Exception | IOException e) {
			throw new IPNOTFOUNDEXCEPTION("Users Ip is not available in Database");
		}
	}

}
