package com.workshycoder.geo.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.workshycoder.geo.exception.IPNOTFOUNDEXCEPTION;
import com.workshycoder.geo.model.UserDetails;
import com.workshycoder.geo.service.GeoService;

@RestController
public class GeoController {

	@Autowired
	private GeoService geoService;

	@RequestMapping(value = "/getAddress", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<?> getAddress(@RequestParam(value = "ip", required = false, defaultValue = "") String ip,
			HttpServletRequest request) {
		if ("".equals(ip)) {
			ip = request.getRemoteAddr();
		}
		UserDetails userDetails;
		try {
			userDetails = geoService.getUsersAddress(ip);
		} catch (IPNOTFOUNDEXCEPTION e) {
			return new ResponseEntity<>("Unable to determine Users location", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(userDetails, HttpStatus.OK);
	}

}
