package com.workshycoder.geo.service;

import com.workshycoder.geo.exception.IPNOTFOUNDEXCEPTION;
import com.workshycoder.geo.model.UserDetails;

public interface GeoService {
	
	public UserDetails getUsersAddress(String ip) throws IPNOTFOUNDEXCEPTION;

}
