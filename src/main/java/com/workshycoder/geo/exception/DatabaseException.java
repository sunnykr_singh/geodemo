package com.workshycoder.geo.exception;

import java.io.IOException;

public class DATABASEEXCEPTION extends IOException{
	
	private static final long serialVersionUID = 1L;
	
	public DATABASEEXCEPTION(String message) {
		System.out.println("Error occured while building DataBaseReader");
	}

}
