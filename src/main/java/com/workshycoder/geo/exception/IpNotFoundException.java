package com.workshycoder.geo.exception;

public class IPNOTFOUNDEXCEPTION extends Exception{

	private static final long serialVersionUID = 1L;
	
	public IPNOTFOUNDEXCEPTION(String message) {
		System.out.println(message);
	}
}
