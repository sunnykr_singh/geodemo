package com.workshycoder.geo.model;

public class UserDetails {

	private String city;
	private String postal;
	private String ip;
	private String latitude;
	private String longitude;

	public UserDetails() {

	}

	public String getCity() {
		return city;
	}

	public UserDetails setCity(String city) {
		this.city = city;
		return this;
	}

	public String getPostal() {
		return postal;
	}

	public UserDetails setPostal(String postal) {
		this.postal = postal;
		return this;
	}

	public String getIp() {
		return ip;
	}

	public UserDetails setIp(String ip) {
		this.ip = ip;
		return this;
	}

	public String getLatitude() {
		return latitude;
	}

	public UserDetails setLatitude(String latitude) {
		this.latitude = latitude;
		return this;
	}

	public String getLongitude() {
		return longitude;
	}

	public UserDetails setLongitude(String longitude) {
		this.longitude = longitude;
		return this;
	}

	@Override
	public String toString() {
		return "UserDetails [city=" + city + ", postal=" + postal + ", ip=" + ip + ", lat=" + latitude + ", lon=" + longitude
				+ "]";
	}

}
